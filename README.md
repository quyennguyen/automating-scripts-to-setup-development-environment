# README

How to use the scripts

Automation script that gonna help us reduce a bunch of work when setting up our development environment at local. Specifically, the script will install and fully configure for you the following :

- Update + upgrade OS: ubuntu 12.04 LTS precise
- web stack LAMP: nginx 1.4.6/apache 2.2 + php 5.4 + mysql 5.5
- Open JDK 1.7
- Subversion 1.7
- memcached + php memcache
- rabbitmq + additional packages
- redis 2.8.7 + phpredis 2.2

Important note: 
It is recommended that we build up our local development in a virtual machine. Some benefits from doing that:

- We get a pure, isolated environment, not affected by other stuff installed in your machine
- Our environment is identical to staging or production.
- We can bring our VM to any other machine.

The idea is that we will build the server in the VM (called `guest`), while our source code is located at the real machine (called `host`) which will be shared to the VM. All our coding, committing will be done in the host, but the server IP is not 127.0.0.1, but is now pointed to the guest IP.

So, all you have to do is walking through these steps:
## 1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## 2. Install [Vagrant](http://www.vagrantup.com/downloads.html). Check vagrant installed:
~~~
$ vagrant --version
Vagrant 1.3.5
~~~

## 3. Install ansible
~~~
$ sudo apt-add-repository ppa:rquillo/ansible
$ sudo apt-get update
$ sudo apt-get install ansible
$ ansible --version
ansible 1.4.4
~~~

## 4. Download the vagrant box [precise-server-cloudimg-amd64-vagrant-disk1.box](http://cloud-images.ubuntu.com/vagrant/precise/current/precise-server-cloudimg-amd64-vagrant-disk1.box)

## 5. Clone my bitbucket project: 
~~~
git clone https://quyennguyen@bitbucket.org/quyennguyen/automating-scripts-to-setup-development-environment.git
~~~

## 6. Prepare your directories as below:
~~~
VM/
├── precise-server-cloudimg-amd64-vagrant-disk1.box
└── provisioning
    └── automating-scripts-to-setup-development-environment   # Directory contains the scripts
~~~

## 8. Initialize the box
~~~
$ cd VM
$ mkdir dev & cd dev
$ vagrant box add lzd.dev ../precise-server-cloudimg-amd64-vagrant-disk1.box   		# adding vagrant box with box name is lzd.dev
$ vagrant box init			# initialize the box. Vagrantfile will be created
$ ls
Vagrantfile
~~~

## 9. Add host file to define the VM's IP
~~~
$ vim host
# Add the following, you may change the IP as you want
[vagrant]
192.168.56.101
~~~

## 10. Edit Vagrantfile
Suppose that the checked out source code will be located at `/home/qnguyen/`. Find the lines, uncomment and edit as below 
~~~
# config.vm.box = “[box-name]”
config.vm.box = “lzd.dev”
 
# config.vm.hostname = "[hostname]" => VM's hostname, will appear as vagrant@[hostname]
config.vm.hostname = "lzd.dev"
 
# config.vm.box_url = "[path/to/.box/file]",
config.vm.box_url = "/home/lazhcm10101/Documents/VM/precise-server-cloudimg-amd64-vagrant-disk1.box"
 
# set IP of the VM
config.vm.network :private_network, "192.168.56.101"
 
# Share folder between the host and the VM
# config.vm.synced_folder, "[path/to/host/folder]", "[path/to/guest/folder]"
config.vm.synced_folder, "/home/qnguyen/", "/var/www/html"

config.vm.provider :virtualbox do |vb|
  #   # Use VBoxManage to customize the VM. For example to change allocated RAM & cpu cores:
      vb.customize ["modifyvm", :id, "--memory", "3072"]
      vb.customize ["modifyvm", :id, "--cpus", "2"]
end

config.vm.provision "ansible" do |ansible|
  # Configure to use which playbook
  ansible.playbook = "/home/lazhcm10101/Documents/bitbucket/automating-scripts-to-setup-development-environment/ansible/install.yml"
  # Define the path to host file
  ansible.inventory_path = "host"
  ansible.verbose = 'vvvv'
  ansible.sudo = true
end
~~~
You may want to take a look at the file `install.yml`, under the roles there are modules that going to be installed. You may remove some module if you don't want it to be installed. However, the `common` module need to be installed because it will update the OS and add some needed repositories for other tools.

## 11. Edit ansible/group_vars/all to change some global variables as you want.

## 12. Final step: call `vagrant up` inside the dev directory
~~~
$ cd VM/dev
$ vagrant up
[default] VM already created. Booting if it's not already running...
[default] Clearing any previously set forwarded ports...
[default] Forwarding ports...
[default] -- 22 => 2222 (adapter 1)
[default] Creating shared folders metadata...
[default] Clearing any previously set network interfaces...
[default] Preparing network interfaces based on configuration...
[default] Booting VM...
[default] Waiting for VM to boot. This can take a few minutes.
[default] VM booted and ready for use!
[default] Configuring and enabling network interfaces...
[default] Mounting shared folders...
[default] -- v-root: /vagrant
[default] -- v-data: /var/www/html
[default] Booting VM...
[default] Waiting for machine to boot. This may take a few minutes...
[default] Machine booted and ready!
[default] Setting hostname...
[default] Configuring and enabling network interfaces...
[default] Mounting shared folders...
[default] -- /vagrant
[default] -- /var/www/html
[default] Running provisioner: ansible...
...
The authenticity of host '192.168.56.101 (192.168.56.101)' can't be established.
ECDSA key fingerprint is 57:b1:e2:e0:d8:1a:35:20:5b:ff:41:6a:9a:a5:ed:bd.
Are you sure you want to continue connecting (yes/no)? yes
~~~
It may take a long time for the command to run (30 minutes or more based on network) because it will download and install all the modules defined in the `install.yml` file. So, seat back and take a cup of coffee!

## 13. When it's done, use the command below to ssh to the VM
~~~
$ cd VM/dev
$ vagrant ssh
~~~

## Troubleshooting
You may want to check out this post when there is some problem with the scripts

[http://quyennt.com/linux-mac-osx/using-vagrant-ansible-to-automate-development-environment-part-2/](http://quyennt.com/linux-mac-osx/using-vagrant-ansible-to-automate-development-environment-part-2/)